# Imagen para ejecutar aplicaciones hechas en Angular

Aplicaciones instaladas:

 - git
 - curl
 - sshpass
 - NodeJS
 - AngulaCLI

## Ejemplo de uso

### Dockerfile


```docker
FROM elcascarudodev/nginx-nodejs-angular

ENV GIT_REPOSITORIO=https://elcascarudo@bitbucket.org/elcascarudo/despliegue-angular-node.git
ENV GIT_PROYECTO=despliegue-angular-node

WORKDIR /var/www/localhost/htdocs

RUN rm -f index.nginx-debian.html

RUN git clone $GIT_REPOSITORIO

WORKDIR /var/www/html/$GIT_PROYECTO
RUN npm install
RUN ng build --prod

RUN cp dist/$GIT_PROYECTO/* /var/www/localhost/htdocs/

WORKDIR /var/www/localhost/htdocs
RUN rm -rf $GIT_PROYECTO

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
```
### docker-compose.yml

```docker
version: '3'

services:
  frontend:
    container_name: ejemplo-angular
    build: .
    restart: always
    ports:
      - '8099:80'
```




## Licencia
[MIT](https://choosealicense.com/licenses/mit/)