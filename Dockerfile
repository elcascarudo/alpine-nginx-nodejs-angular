FROM alpine

LABEL manteiner="elcascarudo.dev@gmail.com"

RUN apk update && \
    apk add git curl sshpass npm nginx

# Configuración para Nginx
COPY nginx/default.conf /etc/nginx/conf.d
RUN mkdir -p /run/nginx
# instalación de AngularCLI para compilar aplicaciones realizadas en Angular
RUN npm install -g @angular/cli